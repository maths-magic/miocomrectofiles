# mIOCOMRecToFiles #

Extract media streams from an IOCOM Rec v2.0 file.

IOCOM's Visimeet video conferencing application [1] allows the recording of meetings to be made on the IOCOM server. The Rec v2.0 format is a container for the stored RTP streams.

This project developed for the MAGIC Taught Course Centre [2] extracts streams from sources that match a specified pattern (i.e. a Visimeet account name) and writes them to separate files in an appropriate media container format.

## Supported RTP Stream Codecs ##

* Audio: Speex 16kz mono in .ogg
* Video: VP8 in .webm

## Dependencies ##

The project makes use of native ogg, vorbis, vpx and webm libraries which have been collated in a separate repository [3] with appropriate JNI wrappers.

## Contributing Projects ##

This project contains individual files from the JSpeex [4] and ViCOVRE [5] projects.

## Resources ##

[1] http://iocom.com

[2] http://maths-magic.ac.uk

[3] https://bitbucket.org/maths-magic/webm

[4] https://sourceforge.net/projects/jspeex/

[5] https://sourceforge.net/projects/vicovre/