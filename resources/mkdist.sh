#!/bin/sh

ARC=mIOCOMRecToFiles.zip

if [ -e $ARC ]; then
  rm -f $ARC
fi

zip -r $ARC extractStreams.sh LICENSE target/*.jar lib/*.so
