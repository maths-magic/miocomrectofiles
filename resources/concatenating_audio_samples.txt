Audio sample seem to get split when the audio is muted. Also other interruptions appear to 
create non-incrementing offset timestamps. To join the audio sample for a single source
conside the following.

To concat sample b to sample a which have offset from | need to compute the delta D
If offset does not increment from the previous sample, eg for sample c: 

 offset(c) < (offset(b)+duration(b))

then assume it is offset from end of sample b at c'

 |----<   a   >
 |----------------------<   b ...>
               <   D   >
 |--<     c     >
                                  |--<    c'    >
