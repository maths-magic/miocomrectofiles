/**
 * Copyright © 2014 Mathematics Access Grid Instruction and Collaboration (MAGIC)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.ac.mathsmagic.iocomrecording2;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.Map;
import uk.ac.mathsmagic.rtp.RTCPPacketFactory;
import uk.ac.mathsmagic.rtp.RTPPacket;
import uk.ac.mathsmagic.rtp.RTCPPacketAPI;
import uk.ac.mathsmagic.rtp.RTCPPacketImpl;
import uk.ac.mathsmagic.rtp.RTCPSDESPacket;
import uk.ac.mathsmagic.rtp.RTPSingleStream;

/**
 * Splits a recording into its streams
 *
 * @author james
 */
public class IOCOMFileReader2 {

  public static final String REC_FILE_EXT = ".rec";
  public static final String IDX_FILE_EXT = ".index";

  private final String recFilename;
  private final String indexFilename;
  private FileInputStream recFile;
  private DataInputStream recInput;
  private FileChannel recChannel;
  private final String[] patterns;

  private FileInputStream indexFile;
  private DataInputStream indexInput;
  private FileChannel indexChannel;

  private IOCOMRecordingHeader header;
  private final IOCOMRecordingEntry entry;

  private final Map<Long, RTPSingleStream> streamsIn;
  private final Map<Long, RTPSingleStream> streamsOut;

  private static final Logger logger = Logger.getLogger(IOCOMFileReader2.class.getName());

  public IOCOMFileReader2(String filename, String[] patterns) {
    recFilename = filename + REC_FILE_EXT;
    indexFilename = filename + IDX_FILE_EXT;
    entry = new IOCOMRecordingEntry();
    streamsIn = new HashMap<>();
    streamsOut = new HashMap<>();
    this.patterns = patterns;
  }

  public Map getStreams() {

    try {
      openFiles();
      readRecHeader();
      readRecEntries();

      streamsIn.size();

      // Filter output streams for those matching patterns
      for (Map.Entry<Long, RTPSingleStream> e : streamsIn.entrySet()) {
        RTPSingleStream s = e.getValue();

        for (String p : patterns)
          if (s.getCName() != null && s.getCName().contains(p)) {
            streamsOut.put(e.getKey(), s);
          }
      }

      for (Map.Entry<Long, RTPSingleStream> e : streamsOut.entrySet()) {
        RTPSingleStream s = e.getValue();
        logger.info(s.toString());
      }

      closeFiles();
    } catch (IOException e) {
      logger.severe(e.getMessage());
    }

    return streamsOut;
  }

  private void openFiles() throws IOException {
    recFile = new FileInputStream(recFilename);
    recInput = new DataInputStream(recFile);
    recChannel = recFile.getChannel();

    indexFile = new FileInputStream(indexFilename);
    indexInput = new DataInputStream(indexFile);
    indexChannel = indexFile.getChannel();
  }

  private void closeFiles() throws IOException {
    recFile.close();
    indexFile.close();
  }

  private void readRecHeader() throws IOException {
    header = new IOCOMRecordingHeader();
    header.duration = recInput.readInt() & RTPPacket.UINT_TO_LONG_CONVERT;
  }

  private void readRecEntry() throws IOException {
    entry.read(recInput);

    if (entry.isRTP()) {
      RTPPacket rtp = new RTPPacket(recInput, entry.getPacketLength(), recChannel.position());
      long ssrc = rtp.getSSrc();

      RTPSingleStream s;
      if (!streamsIn.containsKey(ssrc)) {
        s = new RTPSingleStream();
        s.setAudio(entry.isAudio());
        s.setSsrc(ssrc);
        s.setTimestamp(rtp.getTimestamp());
        s.setOffset(entry.getOffset());
        logger.fine(String.format("SSRC:\t%s\tOffset:\t%s\tAudio:\t%s", ssrc, entry.getOffset(), entry.isAudio()));
        streamsIn.put(ssrc, s);
      } else {
        s = streamsIn.get(ssrc);
        if (s.getTimestamp() == -1) {
          s.setTimestamp(rtp.getTimestamp());
        }
        if (s.getOffset() == -1) {
          s.setOffset(entry.getOffset());
        }
      }

      // Only store packets of streams with no cname yet defined or
      // the cname matches a pattern
      if (s.getCName() == null) {
         s.put(/*rtp.getTimestamp()*/(long) rtp.getSequence(), rtp);
      } else {
        for (String p : patterns) {
          if (s.getCName().contains(p)) {
            s.put(/*rtp.getTimestamp()*/(long) rtp.getSequence(), rtp);
          }
        }
      }
    } else {
      int compoundLength = entry.getPacketLength();
      // Read compound RCTP packets
      while (compoundLength > 0) {
        RTCPPacketAPI rtcp = RTCPPacketFactory.getRTCPPacket(recInput);
        long ssrc = rtcp.getSSrc();

        if (rtcp.getType() == RTCPPacketImpl.RTCP_TYPE_SDES) {
          RTCPSDESPacket sdes = (RTCPSDESPacket) rtcp;

          /* Shouldn't happen but does */
          if (!streamsIn.containsKey(ssrc)) {
            logger.fine(String.format("RTCP before RTP for ssrc %s", ssrc));
            RTPSingleStream s = new RTPSingleStream();
            s.setAudio(entry.isAudio());
            s.setSsrc(ssrc);
            logger.fine(String.format("SSRC:\t%s\tOffset:\t%s\tAudio:\t%s", ssrc, "?? (sdes)", entry.isAudio()));
            streamsIn.put(ssrc, s);
          }

          /* add metadata to stream */
          streamsIn.get(ssrc).mergeSourceDescription(sdes);
        }

        compoundLength -= rtcp.getLength();
      }
    }
  }

  private void readRecEntries() throws IOException {
    boolean eof = false;

    while (!eof) {
      try {
        readRecEntry();
      } catch (EOFException e) {
        eof = true;
      }
    }
  }

  public long getDuration() {
    if (header != null) {
      return header.duration;
    }

    return 0;
  }

  public void readIndexEntries(Map<Long, RTPSingleStream> streams) {
    try {
      int size = indexInput.readInt();
      IOCOMIndexEntry ientry = new IOCOMIndexEntry();

      for (; size > 0; size--) {
        ientry.read(indexInput);
        if (streams.containsKey(ientry.getSSrc())) {
          streams.get(ientry.getSSrc()).putIndex(ientry.getOffset(), ientry.getTimestamp());
        }
      }
    } catch (IOException e) {

    }
  }
}
