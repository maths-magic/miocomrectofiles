/**
 * Copyright © 2014 Mathematics Access Grid Instruction and Collaboration (MAGIC)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.ac.mathsmagic.iocomrecording2;

import java.io.DataInputStream;
import java.io.IOException;
import uk.ac.mathsmagic.rtp.RTPPacket;

/**
 *
 * @author james
 */
public class IOCOMIndexEntry {
  long file_offset; // uint64
  long time_offset; // uint32
  long srcid;


  public void read(DataInputStream indexInput) throws IOException {
    file_offset = readLongLE(indexInput);
    time_offset = readUIntLE(indexInput);
    srcid = readUIntLE(indexInput);
  }

  public long getOffset() {
    return file_offset;
  }

  public long getTimestamp() {
    return time_offset;
  }

  public long getSSrc() {
    return srcid;
  }

  private long readUIntLE(DataInputStream input) throws IOException {
    byte[] byteBuffer = new byte[4];
    input.readFully(byteBuffer);
    return (byteBuffer[3]) << 24 | (byteBuffer[2] & 0xff) << 16 |
            (byteBuffer[1] & 0xff) << 8 | (byteBuffer[0] & 0xff) & RTPPacket.UINT_TO_LONG_CONVERT;
  }

  private long readLongLE(DataInputStream input) throws IOException {
    byte[] byteBuffer = new byte[8];
    input.readFully(byteBuffer);
    return (long)(byteBuffer[7]) << 56 | (long)(byteBuffer[6]&0xff) << 48 |
            (long)(byteBuffer[5] & 0xff) << 40 | (long)(byteBuffer[4] & 0xff) << 32 |
            (long)(byteBuffer[3] & 0xff) << 24 | (long)(byteBuffer[2] & 0xff) << 16 |
            (long)(byteBuffer[1] & 0xff) <<  8 | (long)(byteBuffer[0] & 0xff);
  }

}
