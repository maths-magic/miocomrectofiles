/**
 * Copyright © 2014 Mathematics Access Grid Instruction and Collaboration (MAGIC)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.ac.mathsmagic.iocomrecording2;

import java.io.IOException;
import java.io.DataInputStream;
import uk.ac.mathsmagic.rtp.RTPPacket;

/**
 *
 * @author james
 */
public class IOCOMRecordingEntry {

  private static final int ENTRY_HEADER_LEN = 12;
  private int length; // unint16
  private int packetLength; //uint16
  private boolean isAudio; // packed in uint 32
  private long offset;  // uint32
  private boolean isRTP;

  public int getLength() {
    return length;
  }

  public int getPacketLength() {
    return packetLength;
  }

  public long getOffset() {
    return offset;
  }

  public boolean isAudio() {
    return isAudio;
  }

  public boolean isRTP() {
    return isRTP;
  }

  public void read(DataInputStream recInput) throws IOException {
    this.length = recInput.readShort() & RTPPacket.USHORT_TO_INT_CONVERT;
    this.packetLength = recInput.readShort() & RTPPacket.USHORT_TO_INT_CONVERT;
    this.isAudio = recInput.readByte()==1;
    // robustly skip 3 bytes
    recInput.readByte();
    recInput.readByte();
    recInput.readByte();
    this.offset = recInput.readInt() & RTPPacket.UINT_TO_LONG_CONVERT;

    isRTP = packetLength > 0; // RTCP
    packetLength = length - ENTRY_HEADER_LEN;
  }

  /*
  * return: size ib bytes of this object on disk
  */
  public int getSize() {
    return ENTRY_HEADER_LEN;
  }
}
