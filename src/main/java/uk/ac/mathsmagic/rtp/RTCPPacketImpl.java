/**
 * Copyright © 2014 Mathematics Access Grid Instruction and Collaboration (MAGIC)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.ac.mathsmagic.rtp;

import java.io.DataInputStream;
import java.io.IOException;

/**
 *
 * @author james
 */
public abstract class RTCPPacketImpl implements RTCPPacketAPI {

  /* RTCP Packet Types */
  public static final int RTCP_TYPE_SR = 200;
  public static final int RTCP_TYPE_RR = 201;
  public static final int RTCP_TYPE_SDES = 202;
  public static final int RTCP_TYPE_BYE = 203;
  public static final int RTCP_TYPE_APP = 204;

  // RTCP extention http://tools.ietf.org/html/rfc4585
  // These are FeedBack (FB) packets
  public static final int RTCP_TYPE_RTPFB = 205;
  public static final int RTCP_TYPE_PSFB = 206;

  public static final int RTCP_FLAGS_TYPE_MASK = 0x000000ff;

  private int flags;

  private int type;

  private int length;

  private long ssrc;

  private byte[] payload;

  public final void RTCPPacketBase(int flags) {
    this.setFlags(flags);
    this.setType(flags & RTCP_FLAGS_TYPE_MASK);
  }

  @Override
  public void setFlags(int flags) {
    this.flags = flags;
  }

  @Override
  public int getFlags() {
    return flags;
  }

  @Override
  public void setType(int type) {
    this.type = type;
  }

  @Override
  public int getType() {
    return type;
  }

  @Override
  public void setLength(int length) {
    this.length = length;
  }

  @Override
  public int getLength() {
    return length;
  }

  @Override
  public void setSSrc(long ssrc) {
    this.ssrc = ssrc;
  }

  @Override
  public long getSSrc() {
    return ssrc;
  }

  public void readHeader(DataInputStream stream) throws IOException {
    length = stream.readShort() & RTPPacket.USHORT_TO_INT_CONVERT;
    length = (length + 1) * RTPPacket.UINT_SIZE; // convert to bytes
    ssrc = stream.readInt() & RTPPacket.UINT_TO_LONG_CONVERT;
  }

  public void read(DataInputStream stream) throws IOException {
    payload = new byte[length - 8]; // 8 flags+length+ssrc
    if (stream.read(payload, 0, length - 8) != length - 8 ) {
     throw new IOException(String.format("Tried to read %d bytes but got less", length - 8));
    }
  }
}
