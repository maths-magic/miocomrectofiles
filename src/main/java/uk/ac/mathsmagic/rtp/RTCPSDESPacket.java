/**
 * Copyright © 2014 Mathematics Access Grid Instruction and Collaboration (MAGIC)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.ac.mathsmagic.rtp;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author james
 */
public class RTCPSDESPacket extends RTCPPacketImpl {

  public static final int SDES_ITEM_END = 0;
  public static final int SDES_ITEM_CNAME = 1;
  public static final int SDES_ITEM_NAME = 2;
  public static final int SDES_ITEM_EMAIL = 3;
  public static final int SDES_ITEM_PHONE = 4;
  public static final int SDES_ITEM_LOC = 5;
  public static final int SDES_ITEM_TOOL = 6;
  public static final int SDES_ITEM_NOTE = 7;
  public static final int SDES_ITEM_PRIV = 8;

  private final String[] items = new String[8];
  private final List privItems = new ArrayList();

  public RTCPSDESPacket(int flags) {
    this.RTCPPacketBase(flags);
  }

  @Override
  public void read(DataInputStream stream) throws IOException {
    long sdesItemsLength = this.getLength() - 8;

    while (sdesItemsLength > 0) {
      byte type = stream.readByte();
      if (type == SDES_ITEM_END) {
        if(stream.skip(sdesItemsLength - 1) != sdesItemsLength - 1) {
          throw new IOException(String.format("Tried to read %d bytes but got less", sdesItemsLength - 1));
        }
        sdesItemsLength = 0;
      } else {
        byte length = stream.readByte();
        byte[] value = new byte[length];
        stream.read(value);
        if (type != SDES_ITEM_PRIV) {
          items[type] = new String(value, "UTF-8");
        } else {
          privItems.add(new String(value, "UTF-8"));
        }
        sdesItemsLength -= 2 + length;
      }
    }
  }

  public String[] getItems() {
    return items;
  }

  public List getPrivItems() {
    return privItems;
  }

}
