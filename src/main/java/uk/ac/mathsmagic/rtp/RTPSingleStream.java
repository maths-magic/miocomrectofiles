/**
 * Copyright © 2014 Mathematics Access Grid Instruction and Collaboration (MAGIC)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.ac.mathsmagic.rtp;

import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 *
 * @author james
 */
public class RTPSingleStream {
  /* The source description items */

  private final String[] sdesItems = new String[8];

  /* node name eg Manchester */
  private String nodeName;

  /* eg CAMERA 1, audio */
  private String streamName;

  /* the id of this stream */
  private long ssrc;

  /* the timestamp according to it's own clock when this stream starts */
  private long timestamp = -1;

  /* the offset ms from the start of the recording */
  private long offset = -1;

  /* if main video stream */
  private boolean primary;

  /* if audio */
  private boolean audio = false;

  /* codec eg H264 */
  private String encoding;

  /* video dimensions */
  private int width;
  private int height;

  private int seqoffset = 0;

  /* the rtp packets */
  private SortedMap<Long, RTPPacket> packets = null;
  private SortedMap<Long, Long> index = null;

  public RTPSingleStream() {
    packets = new TreeMap<>();
    index = new TreeMap<>();
    sdesItems[RTCPSDESPacket.SDES_ITEM_CNAME] = null;
  }

  public void put(Long seq, RTPPacket packet) {
    if(seq==0) {
      seqoffset += 65536;
    }

    packets.put(seq+seqoffset, packet);
  }

  public void putIndex(Long offset, Long timestamp) {
    index.put(offset, timestamp);
  }

  public void mergeSourceDescription(RTCPSDESPacket sdes) {
    String[] items = sdes.getItems();

    for (int i = 1; i < 8; i++) {
      if (items[i] != null) {
        sdesItems[i] = items[i];
      }
    }

    List privs = sdes.getPrivItems();
    ListIterator p = privs.listIterator();

    // Search for encoding and nativesize
    while (p.hasNext()) {
      String priv = (String) p.next();

      if (priv.contains("Encoding")) {
        // N00000008EncodingH.264 Medium (CIF)
        encoding = priv.substring(17);

      } else if (!audio && priv.contains("NativeSize")) {
        // N00000008NativeSize352 x 288
        String[] sizes = priv.substring(19).split(" x ");
        width = Integer.parseInt(sizes[0]);
        height = Integer.parseInt(sizes[1]);
      }
    }
  }

  public String getCName() {
    return sdesItems[RTCPSDESPacket.SDES_ITEM_CNAME];
  }

  public String getName() {
    return sdesItems[RTCPSDESPacket.SDES_ITEM_NAME];
  }

  // the NOTE item carries the video stream description
  // is unset for audio
  public String getNote() {
    return sdesItems[RTCPSDESPacket.SDES_ITEM_NOTE];
  }

  public String getNodeName() {
    return nodeName;
  }

  public void setNodeName(String nodeName) {
    this.nodeName = nodeName;
  }

  public String getStreamName() {
    return streamName;
  }

  public void setStreamName(String streamName) {
    this.streamName = streamName;
  }

  public boolean isPrimary() {
    return primary;
  }

  public void setPrimary(boolean primary) {
    this.primary = primary;
  }

  public boolean isAudio() {
    return audio;
  }

  public void setAudio(boolean audio) {
    this.audio = audio;
  }

  @Override
  public String toString() {
    return String.format("%d %s %s %s %s",
            ssrc,
            sdesItems[RTCPSDESPacket.SDES_ITEM_CNAME],
            sdesItems[RTCPSDESPacket.SDES_ITEM_NAME],
            sdesItems[RTCPSDESPacket.SDES_ITEM_NOTE],
            encoding
    );
  }

  public double getFrameRate() {
    return 10; // TODO
  }

  public int getHeight() {
    return height;
  }

  public int getWidth() {
    return width;
  }

  public String getCodec() {
    if(audio)
      return encoding.substring(0, encoding.indexOf('-'));
    else
      return encoding.substring(0, encoding.indexOf(' '));
  }

  public long getSsrc() {
    return ssrc;
  }

  public void setSsrc(long ssrc) {
    this.ssrc = ssrc;
  }

  public long getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(long timestamp) {
    this.timestamp = timestamp;
  }

  public long getOffset() {
    return offset;
  }

  public void setOffset(long offset) {
    this.offset = offset;
  }

  public Map<Long, RTPPacket> getPackets() {
    return packets;
  }

  public long getIndexStart() {
    if(index.size()>0)
      return index.get(index.firstKey());
    else
      return 0L;
  }

  public int getSeqoffset() {
    return seqoffset;
  }

  public void setSeqoffset(int seqoffset) {
    this.seqoffset = seqoffset;
  }
}
