/**
 * Copyright © 2014 Mathematics Access Grid Instruction and Collaboration (MAGIC)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.ac.mathsmagic.rtp;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;

/**
 *
 * @author james
 */
public class RTCPPacketFactory {

  public static RTCPPacketAPI getRTCPPacket(DataInputStream stream) throws EOFException, IOException {
    RTCPPacketImpl rtcp = null;

    int flags = stream.readShort() & RTPPacket.USHORT_TO_INT_CONVERT;
    int type = flags & RTCPPacketImpl.RTCP_FLAGS_TYPE_MASK;

    switch (type) {
      case (RTCPPacketImpl.RTCP_TYPE_SR):
        rtcp = new RTCPSRPacket(flags);
        break;
      case (RTCPPacketImpl.RTCP_TYPE_RR):
        rtcp = new RTCPRRPacket(flags);
        break;
      case (RTCPPacketImpl.RTCP_TYPE_SDES):
        rtcp = new RTCPSDESPacket(flags);
        break;
      case (RTCPPacketImpl.RTCP_TYPE_BYE):
        rtcp = new RTCPBYEPacket(flags);
        break;
      case (RTCPPacketImpl.RTCP_TYPE_APP):
        rtcp = new RTCPAPPPacket(flags);
        break;
      case (RTCPPacketImpl.RTCP_TYPE_RTPFB):
        rtcp = new RTCPRTPFBPacket(flags);
        break;
      case (RTCPPacketImpl.RTCP_TYPE_PSFB):
        rtcp = new RTCPPSFBPacket(flags);
        break;
      default:
        throw new IOException(String.format("RTCP Type invalid; %d", type));
    }

    rtcp.readHeader(stream);
    rtcp.read(stream);

    return rtcp;
  }
}
