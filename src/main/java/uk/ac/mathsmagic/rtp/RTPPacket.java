/**
 * Copyright (c) 2008, University of Bristol
 * Copyright (c) 2008, University of Manchester
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1) Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2) Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3) Neither the names of the University of Bristol and the
 *    University of Manchester nor the names of their
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

package uk.ac.mathsmagic.rtp;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.util.logging.Logger;

/**
 * Represents an RTP packet
 *
 * @author James S Perrin
 *
 * Based on RTPPacket by
 * author Andrew G D Rowley
 * version 2-0-alpha
 */
public class RTPPacket {

    /**
     * The current RTP Header Version
     */
    public static final int VERSION = 2;

    /**
     * The maximum payload type
     */
    public static final int MAX_PAYLOAD = 127;

    /**
     * The size of the RTP Header - less csrcs
     */
    public static final int HEADER_SIZE = 12;

    /**
     * The maximum RTP sequence
     */
    public static final int MAX_SEQUENCE = 65535;

    /**
     * Unsigned int to long conversion mask
     */
    public static final long UINT_TO_LONG_CONVERT = 0x00000000ffffffffL;

    /**
     * Unsigned short to int conversion mask
     */
    public static final int USHORT_TO_INT_CONVERT = 0x0000ffff;


    public static final int UINT_SIZE = 4;

    // The mask to extract the version from a short
    private static final int VERSION_MASK = 0xc000;

    // The shift to extract the version from a short
    private static final int VERSION_SHIFT = 14;

    // The mask to extract the padding from a short
    private static final int PADDING_MASK = 0x2000;

    // The shift to extact the padding from a short
    private static final int PADDING_SHIFT = 13;

    // The mask to extract the extension bit from a short
    private static final int EXTENSION_MASK = 0x1000;

    // The shift to extract the extension bit from a short
    private static final int EXTENSION_SHIFT = 12;

    // The mask to extract the CSRC from a short
    private static final int CSRC_MASK = 0x0f00;

    // The shift to extract the CSRC from a short
    private static final int CSRC_SHIFT = 8;

    // The mask to extract the marker bit from a short
    private static final int MARKER_MASK = 0x0080;

    // The shift to extract the marker bit from a short
    private static final int MARKER_SHIFT = 7;

    // The mask to extract the type from a short
    private static final int TYPE_MASK = 0x007f;

    // The shift to extract the type from a short
    private static final int TYPE_SHIFT = 0;

    // The first 16 bits of the header
    private int flags;

    // The second 16 bits of the header
    private int sequence;

    // The third and fourth 16 bits of the header
    private long timestamp;

    // The fifth and sixth 16 bits of the header
    private long ssrc;

    // array of contributing srcs
    private long[] csrcs;

    // the payload offet in the file
    private long payloadOffset;

    private int payloadLength;

    // The payload
    private byte[] payload;

    private static final Logger logger = Logger.getLogger(RTPPacket.class.getName());
    /**
     * Creates a new RTPPacket
     *
     * @param stream
     *            The stream to parse the packet from
     * @param length The length of the data to read
     * @param position The position on the data
     * @throws IOException
     */
    public RTPPacket(DataInputStream stream, int length, long position)
            throws IOException {
        // Read the header values
        this.flags = stream.readUnsignedShort() & USHORT_TO_INT_CONVERT;
        if( this.getVersion() != 2 || length<1 )
        {
          logger.severe("RTPPacket version not 2");
          throw new InvalidObjectException("RTPPacket version not 2");
        }
        this.sequence = stream.readUnsignedShort() & USHORT_TO_INT_CONVERT;
        this.timestamp = stream.readInt() & UINT_TO_LONG_CONVERT;
        this.ssrc = stream.readInt() & UINT_TO_LONG_CONVERT;
        int cc = getCsrcCount();

        if(cc>0) {
          csrcs = new long[cc];
          for(int c=0; c<cc; c++) {
            csrcs[c] = stream.readInt() & UINT_TO_LONG_CONVERT;
          }
        }

        // TODO: for now skip payload
        // Would be best to store offset, but need to use
        payloadOffset = position + cc*4 + HEADER_SIZE;
        payloadLength = length - cc*4 - HEADER_SIZE;
        payload = new byte[payloadLength];
        if( stream.read(payload, 0, payloadLength) != payloadLength) {
          throw new IOException(String.format("Tried to read %d but got less", payloadLength));
        }
    }

    /**
     * Returns the header flags fields
     * @return The flags of the header
     */
    public int getFlags() {
        return flags;
    }

    /**
     * Returns the RTP version
     * @return The RTP version implemented
     */
    public short getVersion() {
        return (short) ((getFlags() & VERSION_MASK) >> VERSION_SHIFT);
    }

    /**
     * Returns the padding flag value
     * @return The padding in the data of the packet
     */
    public short getPadding() {
        return (short) ((getFlags() & PADDING_MASK) >> PADDING_SHIFT);
    }

    /**
     * Returns the RTP extension header
     * @return Any extension to the header
     */
    public short getExtension() {
        return (short) ((getFlags() & EXTENSION_MASK) >> EXTENSION_SHIFT);
    }

    /**
     * Returns the number of CSRCs in the packet
     * @return A count of Csrcs in the packet
     */
    public short getCsrcCount() {
        return (short) ((getFlags() & CSRC_MASK) >> CSRC_SHIFT);
    }

    /**
     * Returns the marker bit
     * @return The marker of the packet
     */
    public short getMarker() {
        return (short) ((getFlags() & MARKER_MASK) >> MARKER_SHIFT);
    }

    /**
     * Returns the RTP type
     * @return The type of the data in the packet
     */
    public short getPacketType() {
        return (short) ((getFlags() & TYPE_MASK) >> TYPE_SHIFT);
    }

    /**
     * Returns the sequence number
     * @return The sequence number of the packet
     */
    public int getSequence() {
        return sequence;
    }

    /**
     * Returns the packet timestamp
     * @return The timestamp of the packet
     */
    public long getTimestamp() {
        return timestamp;
    }

    /**
     * Returns the source id
     * @return The ssrc of the data source
     */
    public long getSSrc() {
        return ssrc;
    }

    public byte[] getPayload() {
      return payload;
    }
}
