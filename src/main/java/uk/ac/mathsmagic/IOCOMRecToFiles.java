/**
 * Copyright © 2014 Mathematics Access Grid Instruction and Collaboration (MAGIC)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.ac.mathsmagic;

import java.util.HashMap;
import uk.ac.mathsmagic.iocomrecording2.IOCOMFileReader2;
import uk.ac.mathsmagic.media.StreamMediaWriterFactory;
import uk.ac.mathsmagic.media.StreamMediaWriterImpl;
import uk.ac.mathsmagic.rtp.RTPSingleStream;

import java.util.Map;

/**
 * James S. Perrin
 *
 * Take a IOCOM Visimeet recording file V2.0 and extract the all/some of the
 * streams
 *
 */
public class IOCOMRecToFiles {

  /**
   *
   * @param args filename [pattern1[,pattern2]...]
   */
  public static void main(String[] args) {
    String filename = args[0];
    String[] patterns = null;
    long duration;
    Map<Long, RTPSingleStream> streams;

    if (filename.endsWith(IOCOMFileReader2.IDX_FILE_EXT)) {
      filename = filename.substring(0, filename.length()-IOCOMFileReader2.IDX_FILE_EXT.length());
    } else if (filename.endsWith(IOCOMFileReader2.REC_FILE_EXT)) {
      filename = filename.substring(0, filename.length()-IOCOMFileReader2.REC_FILE_EXT.length());
    }

    int a = args.length;
    if (a > 1) {
      patterns = new String[a - 1];

      for (int p = 1; p < a; p++) {
        patterns[p-1] = args[p];
      }
    }

    IOCOMFileReader2 iocomfile = new IOCOMFileReader2(filename, patterns);
    streams = iocomfile.getStreams();
    duration = iocomfile.getDuration();

    // Check for multiple audio streams from the same source and pick the longest
    Map<String, RTPSingleStream> audioStreams  = new HashMap<>();

    for(Map.Entry<Long, RTPSingleStream> e: streams.entrySet()) {
      RTPSingleStream s = e.getValue();
      if (s.isAudio()
              && (!audioStreams.containsKey(s.getCName())
                  || audioStreams.get(s.getCName()).getPackets().size() < s.getPackets().size())) {
        audioStreams.put(s.getCName(), s);
      }
    }

    // mark audio streams for output
    for(Map.Entry<String, RTPSingleStream> e: audioStreams.entrySet()) {
      RTPSingleStream s = e.getValue();
      s.setStreamName("Audio[%d]");
    }

    int vid = 0; // allow streams with the same name to be distinguished
    for(Map.Entry<Long, RTPSingleStream> e: streams.entrySet()) {
      RTPSingleStream s = e.getValue();
      s.setNodeName(s.getName().replace(' ', '_'));

      // Create some usable data from stream data
      if (s.isAudio()) {
        if (s.getStreamName() == null || !s.getStreamName().startsWith("Audio")) {
          continue;
        }
      } else {
        s.setPrimary(s.getNote().contains("___PREFERRED___"));
        s.setStreamName(s.getNote().replace("___PREFERRED___", "").replace(' ', '_'));

        // handle IE stream names that contain URL with un-filename friendly chars
        if (s.getStreamName().contains("Internet_Explorer")) {
          s.setStreamName(String.format("Internet_Explorer_%02d", vid));
        }
      }

      StreamMediaWriterImpl mediaWriter = StreamMediaWriterFactory.getWriter(s);

      if(mediaWriter != null) {
        if(filename.indexOf('/')>=0) {
          filename = filename.substring(filename.lastIndexOf('/')+1);
        }
        String mediaFilename = String.format("%1$s-%2$s-%3$s-%%08d.%4$s",
              s.getNodeName(),s.getStreamName(),filename, mediaWriter.getFileExtension());
        mediaWriter.write(mediaFilename, duration, s.getOffset(), s.getTimestamp());
      }
    }

    vid += 1;
  }
}
