/**
 * Copyright © 2014 Mathematics Access Grid Instruction and Collaboration (MAGIC)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.ac.mathsmagic.media;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author james
 */
public class OggPageHeader implements OggPacket {

  public static final short SHORT_TO_UBYTE = 0x00ff;
  public static final long LONG_TO_UINT = 0x00000000ffffffff;
  public static final byte OGG_HEADER_MOS = 0; // Middle of Stream
  public static final byte OGG_HEADER_BOS = 2; // Beginning of Stream
  public static final byte OGG_HEADER_EOS = 4; // End of Stream

  private static final String MAGIC_BYTES = "OggS"; // must be OggS
  private short version; // ubyte
  private byte header_type = OGG_HEADER_MOS; // ubyte
  private long granule_position = 0; // uint
  private long bitstream_serial_no; // uint
  private final long page_seq_no; // uint
  private long crc_checksum; // uint
  private byte page_segments = 1; // ubyte
  private byte[] segment_table = {0}; // [page_segments]

  public OggPageHeader(byte header_type, long page_seq_no) {
    this.header_type = header_type;
    this.page_seq_no = page_seq_no;
  }

  @Override
  public byte[] toBytes() {
    ByteArrayOutputStream bos = new ByteArrayOutputStream();

    try {
      bos.write(MAGIC_BYTES.getBytes("UTF-8")); // NULL terminator?
      bos.write((byte) (version & SHORT_TO_UBYTE));
      bos.write((byte) (header_type & SHORT_TO_UBYTE));
      bos.write(toBytes(granule_position)); // byte order?
      bos.write(toBytes((int) (bitstream_serial_no & LONG_TO_UINT)));
      bos.write(toBytes((int) (page_seq_no & LONG_TO_UINT)));
      bos.write(toBytes((int) (crc_checksum & LONG_TO_UINT)));
      bos.write(page_segments);
      bos.write(segment_table);
    } catch (IOException e) {

    }

    return bos.toByteArray();
  }

  public void setGranulePosition(long granule_position) {
    this.granule_position = granule_position;
  }

  public void setSegments(byte[] segments) {
    page_segments = (byte) segments.length;
    segment_table = segments;
  }

  public void setBitstreamSerialNo(long bitstream_serial_no) {
    this.bitstream_serial_no = bitstream_serial_no;
  }

  public void setCrcChecksum(long crc_checksum) {
    this.crc_checksum = crc_checksum;
  }

  public static byte[] toBytes(int value) {
    return ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(value).array();
  }

  public static byte[] toBytes(long value) {
    return ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN).putLong(value).array();
  }
}
