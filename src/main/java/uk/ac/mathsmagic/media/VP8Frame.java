/**
 * Copyright © 2014 Mathematics Access Grid Instruction and Collaboration (MAGIC)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.ac.mathsmagic.media;

import java.io.ByteArrayOutputStream;

/**
 *
 * @author james
 */
public class VP8Frame {
  final static int VP8_HEADER_KEY_MASK = 0x80; // inverted 0=key
  private byte header;
  private long timestamp;
  private boolean isKey;

  private final ByteArrayOutputStream bufferStream;

  public VP8Frame(byte header) {
    this.bufferStream = new ByteArrayOutputStream();
    isKey = (header & VP8_HEADER_KEY_MASK) == 0;
  }

  public void addBytes(byte[] arr, int offset) {
    // FIXME, assume packets are unpadded and payload goes to the end of the arr.
    bufferStream.write(arr, offset, arr.length-offset);
  }

  public byte[] getBuffer() {
    return bufferStream.toByteArray();
 }

  public byte getHeader() {
    return header;
  }

  public void setHeader(byte header) {
    this.header = header;
  }

  public long getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(long timestamp) {
    this.timestamp = timestamp;
  }

  public boolean isKey() {
    return isKey;
  }

  public void setIsKey(boolean isKey) {
    this.isKey = isKey;
  }


}
