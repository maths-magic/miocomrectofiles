/**
 * Copyright © 2014 Mathematics Access Grid Instruction and Collaboration (MAGIC)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.ac.mathsmagic.media;

import java.io.ByteArrayOutputStream;

/**
 *
 * @author james
 */
public class OggSpeexDataPacket implements OggPacket {
  public static final int OGG_PAGE_PACKET_SIZE = 4200; // recommend page size 4-8k
  public static final int OGG_PAGE_MAX_PACKETS = 254; // allow room for end of sample 0

  private final ByteArrayOutputStream packetStream;
  private final ByteArrayOutputStream packetSegments;

  OggSpeexDataPacket(){
    packetStream = new ByteArrayOutputStream();
    packetSegments = new ByteArrayOutputStream();
  }

  public void addBytes(byte arr[], int offset) {
    packetStream.write(arr, offset, arr.length-offset);
    packetSegments.write((byte)(arr.length-offset));
  }

  public void addEndPacket() {
    packetSegments.write((byte)0);
  }

  public byte[] getPacketSegments() {
    return packetSegments.toByteArray();
  }

  @Override
  public byte[] toBytes() {
    return packetStream.toByteArray();
  }

}
