/**
 * Copyright © 2014 Mathematics Access Grid Instruction and Collaboration (MAGIC)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.ac.mathsmagic.media;

import java.util.Iterator;
import java.util.logging.Logger;
import java.util.Map.Entry;
import uk.ac.mathsmagic.rtp.RTPPacket;

/**
 *
 * @author james
 */
public class VP8Depacketizer {

  static final int VP8_DESCRIPTOR_X_MASK = 0x80;
  static final int VP8_DESCRIPTOR_S_MASK = 0x10;
  static final int VP8_DESCRIPTOR_I_MASK = 0x80;
  static final int VP8_DESCRIPTOR_L_MASK = 0x40;
  static final int VP8_DESCRIPTOR_T_MASK = 0x20;


  private final Iterator<Entry<Long, RTPPacket>> packetIterator;
  private VP8Frame frame = null;
  private RTPPacket currentPacket = null;
  // private final boolean partitionFound = false;

  private static final Logger logger = Logger.getLogger(VP8Depacketizer.class.getName());

  public VP8Depacketizer(Iterator<Entry<Long, RTPPacket>> packetIterator) {
    this.packetIterator = packetIterator;
    if (packetIterator.hasNext()) {
      currentPacket = packetIterator.next().getValue();
    }
  }

  boolean buildFrame() {
    if (currentPacket == null) {
      return false;
    }

    byte[] payload = currentPacket.getPayload();
    int offset = readPayloadDescriptor(payload);
    byte header = payload[offset];

    logger.fine(String.format("%#04x", payload[0]));

    // First packet must be start of a partition and descriptor MUST have S=1
    /*
    if (!PartitionFound) {
      while ((payload[0] & VP8_DESCRIPTOR_S_MASK) == 0 && packetIterator.hasNext()) {
        logger.fine("Skipping packet S=0");
        currentPacket = packetIterator.next().getValue();
        payload = currentPacket.getPayload();
        offset = readPayloadDescriptor(payload);
        logger.fine(String.format("%#04x", payload[0]));
        header = payload[offset];
        PartitionFound = true;
      }
    }*/

    frame = new VP8Frame(header);
    frame.setTimestamp(currentPacket.getTimestamp());
    frame.addBytes(payload, offset);
    boolean eofr = currentPacket.getMarker() == 1;

    // get the rest of the frame
    while (!eofr && packetIterator.hasNext()) {
      currentPacket = packetIterator.next().getValue();
      eofr = currentPacket.getMarker() == 1;
      payload = currentPacket.getPayload();
      offset = readPayloadDescriptor(payload);
      logger.fine(String.format("%#04x", payload[0]));
      frame.addBytes(payload, offset);
    }

    if (!packetIterator.hasNext()) {
      currentPacket = null;
    } else {
      currentPacket = packetIterator.next().getValue();
    }

    return true;
  }

  VP8Frame getFrame() {
    return frame;
  }

  int readPayloadDescriptor(byte[] payload) {
    int offset = 1;

    if ((payload[0] & VP8_DESCRIPTOR_X_MASK) != 0) {
      offset++;
      if ((payload[1] & VP8_DESCRIPTOR_I_MASK) != 0) {
        offset++;
        if ((payload[1] & VP8_DESCRIPTOR_L_MASK) != 0) {
          offset++;
        }
      }
      if ((payload[1] & VP8_DESCRIPTOR_T_MASK) != 0) {
        offset++;
      }
    }

    return offset;
  }

}
