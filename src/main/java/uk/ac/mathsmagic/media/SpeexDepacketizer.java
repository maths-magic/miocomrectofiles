/**
 * Copyright © 2014 Mathematics Access Grid Instruction and Collaboration (MAGIC)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.ac.mathsmagic.media;

import java.util.Iterator;
import java.util.logging.Logger;
import java.util.Map;
import uk.ac.mathsmagic.rtp.RTPPacket;

/**
 *
 * @author james
 */
class SpeexDepacketizer {
  /* has a payload type of 98 and appears to have clock rate of 8000Hz
   * despite the sample rate being 16k
   */
  private final Iterator<Map.Entry<Long, RTPPacket>> packetIterator;
  private RTPPacket currentPacket = null;
  private OggSpeexDataPacket sdp = null;
  private Boolean eos = false; // end of sample

  private static final Logger logger = Logger.getLogger(SpeexDepacketizer.class.getName());

  public SpeexDepacketizer(Iterator<Map.Entry<Long, RTPPacket>> packetIterator) {
    this.packetIterator = packetIterator;
    if (packetIterator.hasNext()) {
      currentPacket = packetIterator.next().getValue();
    }
  }

  public boolean buildPacket() {
    if (currentPacket == null) {
      return false;
    }

    sdp = new OggSpeexDataPacket();
    int nFrames = 0;

    while(currentPacket != null && nFrames < OggSpeexDataPacket.OGG_PAGE_MAX_PACKETS) {
      long ts = currentPacket.getTimestamp();
      // add frame
      sdp.addBytes(currentPacket.getPayload(), 0);
      if(currentPacket.getMarker() == 1) {
        // This can be missing at the start of a recording
        logger.info(String.format("Start of sample @%f",((float)ts/OggSpeexHeaderPacket.SPEEX_RATE_NB)));
      }

      // check for next packet
      if (packetIterator.hasNext()) {
        currentPacket = packetIterator.next().getValue();
        // Was that the last frame in a sample
        if(currentPacket.getMarker() == 1) {
          sdp.addEndPacket();
          logger.info(String.format("End of sample   @", ((float)ts/OggSpeexHeaderPacket.SPEEX_RATE_NB)));

          // Want to trigger the start of a new file
          eos = true;
          return true;
        }
      } else {
        currentPacket = null;
        sdp.addEndPacket();
      }
      nFrames++;
    }

    eos = false;
    return true;
  }

  public OggSpeexDataPacket getPacket() {
    return sdp;
  }

  public boolean EOStream() {
    return currentPacket == null;
  }

  public boolean EOSample() {
    return eos;
  }

  public long getTimeStamp() {
    if (currentPacket != null) {
      return currentPacket.getTimestamp();
    } else {
      return 0;
    }
  }
}
