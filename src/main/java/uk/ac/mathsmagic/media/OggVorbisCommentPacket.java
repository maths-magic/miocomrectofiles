/**
 * Copyright © 2014 Mathematics Access Grid Instruction and Collaboration (MAGIC)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.ac.mathsmagic.media;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author james
 */
public class OggVorbisCommentPacket implements OggPacket {
  private final static String VENDOR_STRING = "IOCOM Visimeet RTP Stream, repacked by mIOCOMRecToFiles";
  private final int vendor_length = VENDOR_STRING.length();
  private final List user_comment_list = new ArrayList();
  private int user_comment_list_length;

  @Override
  public byte[] toBytes() {
    ByteArrayOutputStream bos = new ByteArrayOutputStream();

    try {
      bos.write(OggPageHeader.toBytes(vendor_length));
      bos.write(VENDOR_STRING.getBytes("UTF-8"));
      user_comment_list_length = user_comment_list.size();
      bos.write(OggPageHeader.toBytes(user_comment_list_length));
    } catch (IOException e) {

    }

    return bos.toByteArray();
  }

  public byte getSize() {
    return (byte)(4+VENDOR_STRING.length()+4);
  }
}
