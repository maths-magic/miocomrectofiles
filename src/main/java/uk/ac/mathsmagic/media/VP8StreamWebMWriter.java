/**
 * Copyright © 2014 Mathematics Access Grid Instruction and Collaboration (MAGIC)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.ac.mathsmagic.media;

import uk.ac.mathsmagic.rtp.RTPSingleStream;
import uk.ac.mathsmagic.rtp.RTPPacket;
import com.google.libwebm.mkvmuxer.MkvWriter;
import com.google.libwebm.mkvmuxer.Segment;
import com.google.libwebm.mkvmuxer.SegmentInfo;
import com.google.libwebm.mkvmuxer.VideoTrack;
import java.util.Iterator;
import java.util.logging.Logger;
import java.util.Map;
import java.util.Map.Entry;

/**
 *
 * @author james
 */
public class VP8StreamWebMWriter extends StreamMediaWriterImpl {
  final static String EXTN = "webm";
  final static int CLOCK_RATE = 90000; // clock rate 90kHz
  final static int TIME_SCALE = 1000000000; // nano seconds

  private static final Logger logger = Logger.getLogger(VP8StreamWebMWriter.class.getName());

  public VP8StreamWebMWriter(RTPSingleStream stream) {
    super(stream);
  }

  @Override
  public String getFileExtension() {
    return EXTN;
  }

  @Override
  public boolean write(String filename, long duration, long offset, long timestampBaseline) {

    StringBuilder error = new StringBuilder();
    MkvWriter writer = new MkvWriter();

    try {
      logger.info(String.format("Writing stream: %s %dx%d to %s", stream.getStreamName(), stream.getWidth(), stream.getHeight(), String.format(filename, offset)));
      writer.open(String.format(filename, offset));

      Segment muxerSegment = new Segment();
      if (!muxerSegment.init(writer)) {
        error.append("Could not initialize muxer segment.");
        return false;
      }

      SegmentInfo muxerSegmentInfo = muxerSegment.getSegmentInfo();
      muxerSegmentInfo.setWritingApp("maths-magic");
      // muxerSegmentInfo.setDuration(duration);
      // FIXME Setting this to the "correct" value stops the frame timestamp being correct!
      // muxerSegmentInfo.setTimecodeScale(TIME_SCALE);

      // Add Audio Track
      long videoTrackNumber = (long) muxerSegment.addVideoTrack(stream.getWidth(), stream.getHeight(), 0);

      VideoTrack muxerTrack = (VideoTrack) muxerSegment.getTrackByNumber(videoTrackNumber);
      if (muxerTrack == null) {
        error.append("Could not get track.");
        return false;
      }

      Map<Long, RTPPacket> packets = stream.getPackets();
      Iterator<Entry<Long, RTPPacket>>packetIterator = packets.entrySet().iterator();
      VP8Depacketizer depacketizer = new VP8Depacketizer(packetIterator);
      int nFrames = 0;
      while (depacketizer.buildFrame()) {
        VP8Frame frame = depacketizer.getFrame();
        byte[] buffer= frame.getBuffer();

        final float timestamp = ((float)(frame.getTimestamp()-timestampBaseline))/(float)CLOCK_RATE;
        final boolean isKey = frame.isKey();
        if (!muxerSegment.addFrame(buffer, videoTrackNumber, (long)((float)TIME_SCALE*timestamp), isKey)) {
          error.append("Could not add frame.");
          return false;
        }
        nFrames++;
      }

      logger.info(String.format("Wrote %d frames", nFrames));

      if (!muxerSegment.finalizeSegment()) {
        error.append("Finalization of segment failed.");
        return false;
      }

    } finally {
      writer.close();
    }

    return true;
  }

}
