/**
 * Copyright © 2014 Mathematics Access Grid Instruction and Collaboration (MAGIC)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.ac.mathsmagic.media;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.logging.Logger;
import java.util.Map;
import java.util.Map.Entry;
import org.xiph.speex.OggCrc;
import uk.ac.mathsmagic.rtp.RTPPacket;
import uk.ac.mathsmagic.rtp.RTPSingleStream;

/**
 *
 * @author james
 */
public class SpeexStreamOggWriter extends StreamMediaWriterImpl {

  private static final String EXTN = "ogg";
  private FileOutputStream fout;
  private int sample = 0;

  private static final Logger logger = Logger.getLogger(SpeexStreamOggWriter.class.getName());

  public SpeexStreamOggWriter(RTPSingleStream stream) {
    super(stream);
  }

  @Override
  public String getFileExtension() {
    return EXTN;
  }

  @Override
  public boolean write(String filename, long duration, long offset, long timestampBaseline) {

    try {
      long serialno = stream.getSsrc();
      long frame_size = createFileWithHeader(filename, offset);
      int page_no = 2;
      int granule_pos = 0;
      byte header_type;
      byte[] packetSegments;

      long previous_timestamp = 0L;

      // Write pages 3-N of Speex data Packets
      OggSpeexDataPacket sdp;
      Map<Long, RTPPacket> packets = stream.getPackets();
      Iterator<Entry<Long, RTPPacket>> packetIterator = packets.entrySet().iterator();
      SpeexDepacketizer depacketizer = new SpeexDepacketizer(packetIterator);

      while (depacketizer.buildPacket()) {
        header_type = depacketizer.EOSample() || depacketizer.EOStream() ? OggPageHeader.OGG_HEADER_EOS : OggPageHeader.OGG_HEADER_MOS;

        OggPageHeader header = new OggPageHeader(header_type, page_no);
        sdp = depacketizer.getPacket();
        packetSegments = sdp.getPacketSegments();
        granule_pos += frame_size * packetSegments.length - (header_type == OggPageHeader.OGG_HEADER_EOS ? 1 : 0);

        header.setGranulePosition(granule_pos);
        header.setSegments(packetSegments);
        header.setBitstreamSerialNo(serialno);

        writePage(header, sdp);
        ++page_no;

        if (depacketizer.EOSample()) {
          closeFile();
          ++sample;

          // re #10, check for non Monotonic timestamp issue
          if (depacketizer.getTimeStamp() < previous_timestamp) {
            offset = 0L;
            timestampBaseline = 0L;
          }
          frame_size = createFileWithHeader(filename, offset + (depacketizer.getTimeStamp()- timestampBaseline)/(OggSpeexHeaderPacket.SPEEX_RATE_NB/1000));
          page_no = 2;
          granule_pos = 0;
        }

        previous_timestamp = depacketizer.getTimeStamp();
      }

    } catch (FileNotFoundException e) {

    } catch (IOException e) {
    } finally {
      closeFile();
    }

    return true;
  }

  private long createFileWithHeader(String filename, long offset) throws IOException {
    logger.info(String.format("Writing stream: %s 0x0 to %s", String.format(stream.getStreamName(), sample), String.format(filename, sample, offset)));
    fout = new FileOutputStream(String.format(filename, sample, offset));

    byte[] pktSizeSegment = new byte[1];

    int mode = OggSpeexHeaderPacket.SPEEX_MODE_WB;
    int channels = 1;

    // Write page 1 Speex Header Packet
    OggPageHeader header1 = new OggPageHeader((byte) 2, 0);
    OggSpeexHeaderPacket shp = new OggSpeexHeaderPacket(mode, channels);

    pktSizeSegment[0] = shp.getSize();
    header1.setSegments(pktSizeSegment);
    header1.setBitstreamSerialNo(stream.getSsrc());

    writePage(header1, shp);

    // Write page 2 (Vorbis) Comment Packet
    OggPageHeader header2 = new OggPageHeader((byte) 0, 1);
    OggVorbisCommentPacket vcp = new OggVorbisCommentPacket();

    pktSizeSegment[0] = vcp.getSize();
    header2.setSegments(pktSizeSegment);
    header2.setBitstreamSerialNo(stream.getSsrc());

    writePage(header2, vcp);

    return shp.getFrameSize();
  }

  private boolean writePage(OggPageHeader header, OggPacket data) throws IOException {
    byte[] headerBuffer = header.toBytes();
    byte[] dataBuffer = data.toBytes();
    int chksum;
    chksum = OggCrc.checksum(0, headerBuffer, 0, headerBuffer.length);
    chksum = OggCrc.checksum(chksum, dataBuffer, 0, dataBuffer.length);
    header.setCrcChecksum(chksum);
    fout.write(header.toBytes());
    fout.write(dataBuffer);
    return true;
  }

  private boolean closeFile() {
    try {
      if (fout != null) {
        fout.close();
      }
      return true;
    } catch (IOException ee) {
      // could not  close file
      return false;
    }
  }
}
