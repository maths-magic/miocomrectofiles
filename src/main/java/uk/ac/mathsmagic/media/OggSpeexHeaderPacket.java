/**
 * Copyright © 2014 Mathematics Access Grid Instruction and Collaboration (MAGIC)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.ac.mathsmagic.media;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 *
 * @author james
 */
public class OggSpeexHeaderPacket implements OggPacket {
  public static final int SPEEX_MODE_NB=0;
  public static final int SPEEX_MODE_WB=1;
  public static final int SPEEX_MODE_UWB=2;
  public static final int SPEEX_RATE_NB=8000;
  public static final int SPEEX_RATE_WB=16000;
  public static final int SPEEX_RATE_UWB=32000;
  public static final int SPEEX_FRAME_SIZE_NB=160;
  public static final int SPEEX_FRAME_SIZE_WB=320;
  public static final int SPEEX_FRAME_SIZE_UWB=640;

  String speex_string = "Speex   "; // 8 chars
  String speex_version = "1.0beta1            "; // 20 chars
  int speex_version_id = -2;
  long header_size = 80;
  int rate = SPEEX_RATE_WB;
  int mode = SPEEX_MODE_WB;
  int mode_bitstream_version = 4;
  int nb_channels = 1;
  int bitrate = -1;
  int frame_size = SPEEX_FRAME_SIZE_WB;
  int vbr = 0;
  int frames_per_packet = 1;
  int extra_headers = 0;
  int reserved1 = 0;
  int reserved2 = 0;

  public OggSpeexHeaderPacket(int mode, int channels) throws IOException {
    this.mode = mode;
    switch(mode) {
      case SPEEX_MODE_NB:
        this.rate = SPEEX_RATE_NB;
        this.frame_size = SPEEX_FRAME_SIZE_NB;
        break;
      case SPEEX_MODE_WB:
        this.rate = SPEEX_RATE_WB;
        this.frame_size = SPEEX_FRAME_SIZE_WB;
        break;
      case SPEEX_MODE_UWB:
        this.rate = SPEEX_RATE_UWB;
        this.frame_size = SPEEX_FRAME_SIZE_UWB;
        break;
      default:
        throw new IOException(String.format("Speex Mode Invalid: %d", mode));
    }
    this.nb_channels = channels;
  }

  @Override
  public byte[] toBytes() {
    ByteArrayOutputStream bos = new ByteArrayOutputStream();

    try {
      bos.write(speex_string.getBytes("UTF-8"));
      bos.write(speex_version.getBytes("UTF-8"));
      bos.write(OggPageHeader.toBytes(speex_version_id));
      bos.write(OggPageHeader.toBytes((int)(header_size & OggPageHeader.LONG_TO_UINT)));
      bos.write(OggPageHeader.toBytes(rate));
      bos.write(OggPageHeader.toBytes(mode));
      bos.write(OggPageHeader.toBytes(mode_bitstream_version));
      bos.write(OggPageHeader.toBytes(nb_channels));
      bos.write(OggPageHeader.toBytes(bitrate));
      bos.write(OggPageHeader.toBytes(frame_size));
      bos.write(OggPageHeader.toBytes(vbr));
      bos.write(OggPageHeader.toBytes(frames_per_packet));
      bos.write(OggPageHeader.toBytes(extra_headers));
      bos.write(OggPageHeader.toBytes(reserved1));
      bos.write(OggPageHeader.toBytes(reserved2));
    } catch (IOException e) {

    }

    return bos.toByteArray();
  }

  public byte getSize() {
    return 80;
  }

  public int getFrameSize() {
    return frame_size;
  }

}
