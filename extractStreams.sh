#!/bin/sh

rtpfile=$1
shift 1
account=$@
project_home=$(dirname "$0")

classpath="${project_home}/target"
jarfile="${project_home}/target/mIOCOMRecToFiles-1.0-SNAPSHOT.jar"
main="uk.ac.mathsmagic.IOCOMRecToFiles"
log_formatter='java.util.logging.SimpleFormatter.format'
log_format='%1$tY-%1$tm-%1$td %1$tH:%1$tM:%1$tS %4$s %3$s - %5$s%6$s%n'
java_mem_opts='-Xms=512M -Xmx=2G'
export LD_LIBRARY_PATH="${project_home}/lib"

java -ea -cp $classpath:$jarfile $java_mem_opts -D$log_formatter="$log_format" $main $rtpfile $account
